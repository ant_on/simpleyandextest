package com.petrosoftinc.test.tests;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.petrosoftinc.test.resources.helpers.DriverHelper;
import com.petrosoftinc.test.resources.listeners.CustomSelenideListnerForAllure;
import com.petrosoftinc.test.resources.pages.MainPage;
import com.petrosoftinc.test.resources.pages.SearchPage;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.*;
import ru.yandex.qatools.allure.model.SeverityLevel;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.setWebDriver;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * Created by home on 01.06.16.
 */

@Listeners({CustomSelenideListnerForAllure.class})
@Title("Tests fo Yandex search page")
@Features("Yandex Search Page")
public class YandexTest {

    private static WebDriver driver;

    @BeforeTest(description = "Configure and create new driver instance, open Yandex page")
    @Severity(SeverityLevel.BLOCKER)
    @Title("Setup.")
    @Description("Configure and create new driver instance, open Yandex page")
    public void setup() throws Exception {
        Configuration.baseUrl = "https://yandex.ua/";
        Configuration.reportsFolder = "/Users/home/TestResults/SelenideScreenshots/";
        Configuration.selectorMode = Configuration.SelectorMode.Sizzle;
        driver = DriverHelper.createWebDriver("chrome");
        setWebDriver(driver);

    }

    @AfterTest(description = "Close browser, kill driver instance")
    @Title("Teardown.")
    @Description("Close browser, kill driver instance.")
    public void teardown() throws Exception {
        driver.close();
        driver.quit();
    }

    @Test(description = "Check elements on main page", enabled = true)
    @Title("Check elements on main page")
    @Issues({@Issue("http://link.to.issue")})
    @TestCaseId("ID_1")
    @Stories("Main page")
    public void checkElementsOnMainPage() throws Exception {
        open("https://yandex.ua/");
        MainPage.checkElementsVisibility();
    }

    @Test(description = "Find text \"PetrosoftInc\"", enabled = true, dependsOnMethods = "checkElementsOnMainPage")
    @Title("Find text \"PetrosoftInc\"")
    @Issues({@Issue("http://link.to.issue")})
    @TestCaseId("ID_2")
    @Stories("Search")
    public void trySearch() throws Exception {
        MainPage
                .findText("PetrosoftInc")
                .checkElementsVisibility()
                .clickResultLinkByIndex(1);
        DriverHelper.switchToTabByTitle("End-to-End Retail Automation for the Retail and Petroleum Industries");
        DriverHelper.switchToTabByIndex(1);
    }

    @Test(description = "Login to Yandex", enabled = true, dependsOnMethods = "trySearch")
    @Title("Login as existed user, logout")
    @Issues({@Issue("http://link.to.issue")})
    @TestCaseId("ID_2")
    @Stories("Login/Logout")
    public void loginToYandex() throws Exception {
        SearchPage
                .loginAs("my.ya.test.mail", "myyatestmail")
                .goToMainPage()
                .userShouldBeLoggedIn("my.ya.test.mail")
                .logout();
    }
}
