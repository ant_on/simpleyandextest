package com.petrosoftinc.test.resources.helpers;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.switchTo;

/**
 * Created by home on 01.06.16.
 */
public class DriverHelper {

    @Step("Create driver for {0} browser")
    public static WebDriver createWebDriver(String browserName) throws Exception {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setJavascriptEnabled(true);
        WebDriver driver;
        switch (browserName) {
            case "firefox": driver = new org.openqa.selenium.firefox.FirefoxDriver(capabilities); break;
            case "chrome": driver = new org.openqa.selenium.chrome.ChromeDriver(capabilities); break;
            case "phantomjs": driver = new org.openqa.selenium.phantomjs.PhantomJSDriver(capabilities); break;
            default: driver = new org.openqa.selenium.chrome.ChromeDriver(capabilities); break;
        }
        driver.manage().window().maximize();
        return driver;
    }

    @Step("Switch to tab with title {0}")
    public static void switchToTabByTitle(String title) throws Exception {
        switchTo().window(title);
    }

    @Step("Switch to {0} tab")
    public static void switchToTabByIndex(int index) throws Exception {
        switchTo().window(index-1);
    }
}
