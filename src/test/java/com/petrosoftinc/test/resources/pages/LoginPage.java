package com.petrosoftinc.test.resources.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

/**
 * Created by home on 02.06.16.
 */
public class LoginPage {
    @Step("Login as {0}/{1}")
    public static SearchPage loginAs(String login, String password) {
        $("#login").setValue(login);
        $("#passwd").setValue(password);
        $(".js-submit-button").click();
        return page(SearchPage.class);
    }

}
