package com.petrosoftinc.test.resources.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;

/**
 * Created by home on 01.06.16.
 */
public class SearchPage {
    private static SelenideElement text_input = $(".input__control");
    private static SelenideElement search_button = $(".websearch-button");
    @FindBy(css = ".content__left .serp-list .serp-item")
    private static ElementsCollection results;
    @FindBy(css = ".serp-user")
    private static SelenideElement user_login;
    @FindBy(css = ".logo__link")
    private static SelenideElement logo;
    @FindBy(css = ".navigation__item_name_www")
    private static SelenideElement navigation_search;
    @FindBy(css = ".navigation__item_name_images")
    private static SelenideElement navigation_images;
    @FindBy(css = ".navigation__item_name_video")
    private static SelenideElement navigation_video;
    @FindBy(css = ".navigation__item_name_maps")
    private static SelenideElement navigation_maps;
    @FindBy(css = ".navigation__item_name_market")
    private static SelenideElement navigation_market;
    @FindBy(css = ".navigation__more")
    private static SelenideElement navigation_more;



    @Step("Click on {0} result link")
    public static void clickResultLinkByIndex(int i) {
        results.get(i-1).$("a.serp-item__title-link").click();
    }

    @Step("Login as {0}/{1}")
    public static SearchPage loginAs(String login, String password) {
        user_login.click();
        LoginPage.loginAs(login, password);
        SearchPage.userShouldBeLoggedIn(login);
        return page(SearchPage.class);
    }

    @Step("Check that user {0} is logged in")
    public static SearchPage userShouldBeLoggedIn(String login) {
        $("a.user[data-bem*=\"" + login + "\"]").shouldBe(Condition.visible);
        return page(SearchPage.class);
    }

    @Step("Logout from Yandex account")
    public static SearchPage logout() {
        $(".user__name").click();
        $(".multi-auth__logout").waitUntil(Condition.exist, 10).click();
        SearchPage.user_login.shouldBe(Condition.visible);
        return page(SearchPage.class);
    }

    @Step("Check that all main elements are present on search page")
    public static SearchPage checkElementsVisibility() throws Exception {
        logo.shouldBe(visible);
        navigation_search.shouldBe(visible);
        navigation_images.shouldBe(visible);
        navigation_video.shouldBe(visible);
        navigation_maps.shouldBe(visible);
        navigation_market.shouldBe(visible);
        navigation_more.shouldBe(visible);
        return page(SearchPage.class);

    }

    @Step("Go to main page")
    public static MainPage goToMainPage() throws Exception{
        logo.click();
        return page(MainPage.class);
    }
}
