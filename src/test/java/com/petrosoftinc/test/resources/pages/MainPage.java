package com.petrosoftinc.test.resources.pages;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.page;

/**
 * Created by home on 01.06.16.
 */
public class MainPage {
    private static SelenideElement logo = $(".home-logo");
    private static SelenideElement weather = $(".weather");
    private static SelenideElement region = $(".region");
    private static SelenideElement tvafisha = $(".tvafisha");
    private static SelenideElement services = $(".services");
    private static SelenideElement topnews = $("#wd-wrapper-_topnews");
    private static SelenideElement text = $("#text");
    private static SelenideElement search_button = $(".search2__button");
    @FindBy(css = "div.domik2__slider span.username")
    private static SelenideElement username_span;


    @Step("Find text {0}")
    public static SearchPage findText(String str) {
        text.setValue(str).sendKeys(Keys.RETURN);
        return page(SearchPage.class);
    }

    @Step("Check that all main elements are present on main page")
    public static MainPage checkElementsVisibility() throws Exception {
        logo.shouldBe(visible);
        topnews.shouldBe(visible);
        weather.shouldBe(visible);
        region.shouldBe(visible);
        tvafisha.shouldBe(visible);
        text.shouldBe(visible);
        search_button.shouldBe(visible);
        return page(MainPage.class);
    }

    @Step("Check that user is looged in")
    public static MainPage userShouldBeLoggedIn(String login) throws Exception {
        username_span.shouldHave(text(login.substring(1)));
        return page(MainPage.class);
    }

    @Step("Logout")
    public static MainPage logout() throws Exception {
        username_span.click();
        $(byText("Выйти")).waitUntil(exist, 5).click();
        $(byText("Войти")).shouldBe(visible);
        return page(MainPage.class);
    }
}
