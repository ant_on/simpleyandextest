package com.petrosoftinc.test.resources.listeners;

/**
 * Created by home on 01.06.16.
 */
import com.codeborne.selenide.Screenshots;
import com.codeborne.selenide.logevents.EventsCollector;
import com.codeborne.selenide.logevents.LogEvent;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.codeborne.selenide.logevents.SimpleReport;
import com.google.common.base.Joiner;
import com.google.common.io.Files;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Iterator;
import java.util.logging.Logger;

import static com.codeborne.selenide.WebDriverRunner.source;

public class CustomReport {
    private static final Logger log = Logger.getLogger(SimpleReport.class.getName());
    private EventsCollector logEventListener;

    public CustomReport() {
    }

    public void start() {
        SelenideLogger.addListener(this.logEventListener = new EventsCollector());
    }

    public void finish(String title) {
        SelenideLogger.removeListener(this.logEventListener);
        StringBuilder sb = new StringBuilder();
        sb.append("Report for ").append(title).append('\n');
        String delimiter = '+' + Joiner.on('+').join(this.line(50), this.line(60), new Object[]{this.line(6), this.line(6)}) + "+\n";
        sb.append(delimiter);
        sb.append(String.format("|%-50s|%-60s|%-6s|%-6s|%n", new Object[]{"Element", "Subject", "Status", "ms."}));
        sb.append(delimiter);
        Iterator var4 = this.logEventListener.events().iterator();

        while (var4.hasNext()) {
            LogEvent e = (LogEvent) var4.next();
            sb.append(String.format("|%-50s|%-60s|%-6s|%-6s|%n", new Object[]{e.getElement(), e.getSubject(), e.getStatus(), Long.valueOf(e.getDuration())}));
        }

        sb.append(delimiter);
        log.info(sb.toString());
        attachText(sb.toString());
    }

    public void finishWithFail(String title) {
        SelenideLogger.removeListener(this.logEventListener);
        StringBuilder sb = new StringBuilder();
        sb.append("Report for ").append(title).append('\n');
        String delimiter = '+' + Joiner.on('+').join(this.line(50), this.line(60), new Object[]{this.line(6), this.line(6)}) + "+\n";
        sb.append(delimiter);
        sb.append(String.format("|%-50s|%-60s|%-6s|%-6s|%n", new Object[]{"Element", "Subject", "Status", "ms."}));
        sb.append(delimiter);
        Iterator var4 = this.logEventListener.events().iterator();

        while (var4.hasNext()) {
            LogEvent e = (LogEvent) var4.next();
            sb.append(String.format("|%-50s|%-60s|%-6s|%-6s|%n", new Object[]{e.getElement(), e.getSubject(), e.getStatus(), Long.valueOf(e.getDuration())}));
        }

        sb.append(delimiter);
        log.info(sb.toString());
        attachText(sb.toString());
        try {
            screenshot();
        } catch (IOException e) {
            e.printStackTrace();
        }
        attachHTML();
    }

    private String line(int count) {
        return Joiner.on("").join(Collections.nCopies(count, "-"));
    }

    @Attachment(value = "Log")
    public String attachText(String text) {
        return text;
    }

    @Attachment(type = "image/png", value = "Screenshot")
    public byte[] screenshot() throws IOException {
        File screenshot = Screenshots.takeScreenShotAsFile();
        return Files.toByteArray(screenshot);
    }

    @Attachment(type = "text/html", value = "HTML source")
    public String attachHTML() {
        String page_source = source();
        return page_source;
    }
}


