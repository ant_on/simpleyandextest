package com.petrosoftinc.test.resources.listeners;

/**
 * Created by home on 01.06.16.
 */
import org.testng.ITestResult;
import org.testng.reporters.ExitCodeListener;

public class CustomSelenideListnerForAllure extends ExitCodeListener {
    protected CustomReport report = new CustomReport();

    public CustomSelenideListnerForAllure() {
    }

    public void onTestStart(ITestResult result) {
        this.report.start();
    }

    public void onTestFailure(ITestResult result) {
        this.report.finishWithFail(result.getName());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        this.report.finishWithFail(result.getName());
    }

    public void onTestSuccess(ITestResult result) {
        this.report.finish(result.getName());
    }
}
