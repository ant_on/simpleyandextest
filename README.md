**Simple Yandex test with Selenide+Allure**

How to build report:

1. Open project folder in terminal
2. Execute ```mvn clean test -Dtest=YandexTest site jetty:run```
3. After text execution open http://0.0.0.0:8080/ in browser
